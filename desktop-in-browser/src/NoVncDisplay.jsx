// Example React component for noVNC
import React, { useEffect } from 'react';

const NoVncDisplay = ({ password }) => {
  useEffect(() => {
    const initNoVNC = async () => {
      const RFB = await import('@novnc/novnc/core/rfb.js');
      
      var rfb = new RFB.default(document.getElementById('noVNCView'), 'ws://localhost:6080/websockify', {
        credentials: {
          password: password,
        },
      });

      rfb.addEventListener('connect',  () => {
        console.log('Connected');
      });

      rfb.scaleViewport = true; // Example of setting an option, adjust as needed
    };

    initNoVNC();
  }, [password]);

  return <div id="noVNCView" style={{ width: '100%', height: '100vh' }}></div>;
};

export default NoVncDisplay;
