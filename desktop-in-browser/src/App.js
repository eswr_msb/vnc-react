import NoVncDisplay from './NoVncDisplay';

function App() {
  return (
    <div className="App">
      <h1>GURU noVNC React Integration</h1>
      <NoVncDisplay password="alpine" />
    </div>
  );
}

export default App;
